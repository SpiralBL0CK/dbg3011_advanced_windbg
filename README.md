# Slides and materials for students for the "Debuggers 3011 - Advanced WinDbg" course

* `slides/`
    * `XX-whatever.pdf`: slides deck for a given video
    * `dbg3011-advanced-windbg.pdf`: full slides deck (merging all `XX-whatever.pdf`)
* `tools/`
    * `setup_vm.py`: script to install tools automatically on the Debugger VM and Target VM
    * `ghidra_project/` and `IEUser/`: ret-sync configuration files
    * `ssh-config/`: private/public SSH keys to use between the Debugger VM and Target VM
    * `hello_world.gar`: Ghidra project archive
    * `visual_studio_labs.zip`: Labs/exercises to solve
    * `*.bat` and `dbg-prep.cmd`: WinDbg starting scripts
* `subtitles/`: fixed subtitles for the course's videos